#!/bin/bash

# get the current dir, taken from: http://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

OPTIONAL_DIRS=""

# override this in params.sh to use nvidia-docker
DKR_CMD=docker

# read in the mappings for the various mapped directories
source $DIR/../cfg/params.sh

# Needed for xquartz on OSX
export IP=$(ifconfig $INTERFACE | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
xhost + $IP

set -e
echo "Local ROS directory: $LOCAL_ROS_ROOT"
echo "Local data directory: $LOCAL_DATA_ROOT"
echo "Local workspace directory: $LOCAL_WORKSPACE_ROOT"

# PyCharm
PYCSETTINGS=.PyCharm2017.2/
PYAPP=.local/share/JetBrains/Toolbox/apps/PyCharm-P/ch-0/172.3544.44
JETBRAINS=.java/.userPrefs/jetbrains

# need to map over X11 stuff in order to run graphical programs
XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
GIT=$HOME/.git
GITIGNORE=$HOME/.gitignore
GITCONFIG=$HOME/.gitconfig
SSH=$HOME/.ssh

#######################################################################
# Add any optional directories that should be mounted into the container.
# They will only be mapped if the exist

OPTIONAL_DIRS="$OPTIONAL_DIRS $HOME/.vim"
OPTIONAL_DIRS="$OPTIONAL_DIRS $HOME/.vimrc"

EXTRA_DIRS=""
for OPTIONAL_DIR in $OPTIONAL_DIRS; do
  # Check for existance so as to avoid creating empty dirs on the host
  # if there wasn't anything there to begin with.
  if [ -e "$OPTIONAL_DIR" ]; then
    EXTRA_DIRS="$EXTRA_DIRS -v $OPTIONAL_DIR:$OPTIONAL_DIR"
    echo "Mapping $OPTIONAL_DIR"
  fi
done

#######################################################################
# At present the port mappings are not being used as --net=host is set

PORT_MAPPINGS=""

# Forward jupyter notebook port
PORT_MAPPINGS="8888:8888 $PORT_MAPPINGS"

# Forward for tensorboard
PORT_MAPPINGS="6006:6006 12345:12345 $PORT_MAPPINGS"

# Forward for ROS
PORT_MAPPINGS="11311:11311 $PORT_MAPPINGS"

HOST_MACHINE=127.0.0.1
PORT_MAPPING_ARGS=""
for port_mapping in $PORT_MAPPINGS
do
  IFS=':' read -a ports <<< "$port_mapping"
  if ! nc -z $HOST_MACHINE ${ports[0]}
  then
    PORT_MAPPING_ARGS="-p $HOST_MACHINE:$port_mapping $PORT_MAPPING_ARGS"
  else
    echo "Warning: ${ports[0]} on host machine is not free. Cannot bind to docker container."
  fi
done
#######################################################################

# List all containers, including stopped ones, which contain bento_docker.
# images is a list of container names which are being explicitly set as
# bento_docker_[num_images]
# It may be desirable to purge containers from time to time to get rid
# of the stopped containers: docker container prune
images=`docker ps -a | grep bento_docker | awk 'NF>1{print $NF}'`
num_images=`echo $images | wc -w | awk '{ gsub(/^[ \t]+|[ \t]+$/, ""); print }'`
name="bento_docker_$num_images"
echo $name

# Note that at present nvidia isn't being handled. Need to launch with
# nvidia-docker instead for that.

if [ -z ${IMG_PATH+x} ]; then
    echo "Path not set, using default."
    IMG_PATH="\$PATH"
fi

CONTAINER_HOME=/home/$USER

# by default dkr_cmd is defined as docker at the top of the file but can be overridden in params.sh
$DKR_CMD run -ti \
  -v $XSOCK:$XSOCK \
  -v $XAUTH:$XAUTH \
  -v $GIT:$GIT \
  -v $GITIGNORE:$GITIGNORE \
  -v $GITCONFIG:$GITCONFIG \
  -v $SSH:$SSH \
  $EXTRA_DIRS \
  -v $LOCAL_ROS_ROOT:$CONTAINER_HOME/ros \
  -v $LOCAL_DATA_ROOT:$CONTAINER_HOME/data \
  -v $LOCAL_WORKSPACE_ROOT:$CONTAINER_HOME/workspace \
  -v /dev:/dev \
  -e XAUTHORITY=$XAUTH \
  -e DISPLAY=$DISPLAY \
  -e QT_X11_NO_MITSHM=1 \
  -w $CONTAINER_HOME \
  --add-host="$(hostname -s):127.0.1.1" \
  --hostname="$(hostname -s)" \
  --name $name \
  --privileged \
  --net=host \
  bento_docker bash -c 'export PATH='"$IMG_PATH"'; bash'
