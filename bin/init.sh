#!/usr/bin/env bash

# create the ROS src directory if it does not already exist
cd ~/ros
mkdir -p src
cd src/

# clone the bento repos if they do not already exist
[[ -d adc_msgs ]] || git clone git@bitbucket.org:adaptiveprostheticsgroup/adc_msgs.git
[[ -d bento ]] || git clone git@bitbucket.org:adaptiveprostheticsgroup/bento.git

# build the ROS workspace
cd ..
catkin_make
