# bento_docker
 
This is a convenience repo setup to build an ubuntu image with ROS, gazebo and bento. 
It consists of the main Dockerfile specifying how to generate the docker image as well as a 
number of scripts to try and make using it more convenient.
 
## Building and first run
 
1. `build_bento_docker`. This script will create a user with the same name as your logged in user which you use to
run the script. This user will have sudo rights, but there is no password.
2. configure `cfg/params.sh` as described below.
3. `run_bento_docker`
4. Inside the container run `~/init.sh`. This only needs to be run once (as long as your LOCAL_ROS_ROOT stays intact
you do not need to rerun this even if you rebuild the docker image). This step sets up the ROS directory, clones the 
bento repos into the ROS workspace and builds your ROS workspace.
5. You should now be able to launch `> roscore`. 

### `cfg/params.sh`

The setup assumes that three volumes will be mapped between the host computer and the container (any changes in these
directories from either the host or a running container will be immediately shared between the two). 
These can be configured in `cfg/params.sh`. These mappings are as follows:

- LOCAL_ROS_ROOT->/home/${user}/ros : This is the ROS workspace. Add new ROS packages in the `src` directory.
- LOCAL_DATA_ROOT->/home/${user}/data : This is a data directory. Feel free to dump anything you like here. I use it 
for saving experimental data. When used this way it should NOT be connected to a git repository. NEVER save data in git.
You will eventually really regret it (this is from experience). Also, note that an environment variable `DATA_DIR` is
set to point to this directory inside the container --- you can read it in your programs so you know where to toss
experiment data programmatically.
- LOCAL_WORKSPACE_ROOT->/home/${user}/workspace : Add arbitrary non-ROS projects here. I'm using this to add python 
packages which I then install using `pip install --user --editable .`
- INTERFACE->This is just used for OSX. It's needed for xquartz to enable graphical displays. This isn't 100% 
understood, but we think it should be set to your ethernet device, use the name from running `ifconfig`.
- DKR_CMD->Uncomment this line to enable using the NVIDIA GPU (must install nvidia-docker first).

### OSX

To get graphical displays working on Mac requires a few extra steps since Xauth works differently.

1. `brew install xquartz`
2. `open -a xquartz`
3. Press `cmd` + `,` to open Preferences
4. Check "Allow connections from network clients"


## Saving Changes

The script `save_bento_docker` will find all running bento_docker containers and ask if you want to save them. If you
say yes then you have the option to specify the image name as `image_name:tag`. By default the image is saved as
`bento_docker:latest` so that the next time the bento_docker image is launched it will use your save.

## Customizing

So far there isn't a great way to customize the image. Right now you'd have to tweak the various scripts and the 
Dockerfile. The only thing thing that's easily modified is the locations of your directories via the `params.sh` file.

That being said, you can build the default image, run it and make changes internally then use the `save_bento_docker`
script. Your changes to the image will then be saved, but will be overridden if you run `build_bento_docker` again. 

## sudo

The user build creates a user with same user name as the user which launch the build. The user has sudo rights inside
the container. However, there is NO password set... that combined with the `--privileged` flag used to launch the 
container is dangerous. It may be wise to set a password at least.

## Hardware

Hardware is mapped in such a way as to support plug and play of any devices. These do not need to be present at the 
time the container is started. This is achieved using `--privileged` argument in `run_bento_docker`. 
Note that from a security perspective this is dangerous in that container isn't really sandboxed anymore. If this 
is unacceptable you can explicitly specify devices to mount, but they must then be present at the time the container
is started.


## NVIDIA

Install nvidia-docker: https://github.com/NVIDIA/nvidia-docker . Then in params.sh uncomment the line `export dkr_cmd=nvidia-docker`.


## Networking

The default way to launch docker places the container on the bridge network. In this setting each port has to be
explicitly exposed to the host in the `docker run` command. This is fine if all your ROS connections are going to be 
done within the bridge network, but not if you want nodes to be able to communicate from the host machine or between
machines. There are several ways that this could potentially be addressed (see macvlan) but the easiest I've found
is use `--net=host` in the `docker run` command. This way the host and the container are all operating directly on
the same ports and IP. 

To properly handle communication between multiple machines you will need to configure ROS_IP, ROS_HOSTNAME, 
ROS_MASTER_URI as per the usual ROS networking instructions. You will need to do this in the container for sure and
also on the host machine if you have ROS installed directly on your host OS and want it to talk to your container.

## Pycharm

Pycharm can be run externally from the container --- on your host machine. Simply point it at the directory pointed to
by LOCAL_ROS_ROOT. However, to use the debugger and link to the correct libraries you need to run from inside
the container.

## SSH keys and git
The image will mount your git config directory and your .ssh directory.

## Files

It may be convenient to add the `bin` directory to your path. This way you can run the scripts from anywhere directory.

### build_bento_docker

Builds the image.

### run_bento_docker

Launches a new bento_docker container.

### connect_bento_docker

Connects a new terminal to a running bento_docker container.

### stop_bento_docker

Containers can be stopped from inside their terminal by using `ctrl-D`, but that only stops the open terminal. Use this
script to conveniently stop all bento_docker containers and connected terminals.

### save_bento_docker

Saves a snapshot image of the currently running containers. Default is to save with the name `bento_docker:latest`.

### init.sh

Run from inside the container the first time your ROS workspace is created.

### Dockerfile

Tells docker how to build the image.

# TODO: 
- install other repos, python, etc.
- source setup.bash
- test pycharm
- get python3 working and pip3

