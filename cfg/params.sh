#!/usr/bin/env bash
# Edit these locations to specify your own host machine directories.
export LOCAL_ROS_ROOT=/home/craig/workspace/ros
export LOCAL_DATA_ROOT=/home/craig/workspace/data
export LOCAL_WORKSPACE_ROOT=/home/craig/workspace/dockerspace

# Set this to the name of your ethernet adapter. Needed for xquartz
export INTERFACE=enp3s0


OPTIONAL_DIRS="$OPTIONAL_DIRS $HOME/.java/.userPrefs"

# pycharm settingsS
# In my host I've created a symbolic link:
# > cd ~
# > ln -s .local/share/JetBrains/Toolbox/apps/PyCharm-P/ch-0/172.3968.37/ pycharm
OPTIONAL_DIRS="$OPTIONAL_DIRS $HOME/pycharm"
OPTIONAL_DIRS="$OPTIONAL_DIRS $HOME/.PyCharm2017.2"

# can add things to the path like:
# export IMG_PATH="\$HOME/pycharm/bin:\$PATH"
# Should always supply a default

export IMG_PATH="\$HOME/pycharm/bin:\$PATH"

# To use NVIDIA gpus install nvidia-docker https://github.com/NVIDIA/nvidia-docker and uncomment the line below
export DKR_CMD=nvidia-docker
