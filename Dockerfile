FROM osrf/ros:kinetic-desktop-full

ARG user
ARG uid
ARG hostname

#ENV USERNAME thor

#RUN useradd -m $USERNAME && \
#        echo "$USERNAME:$USERNAME" | chpasswd && \
#        usermod --shell /bin/bash $USERNAME && \
#        usermod -aG sudo $USERNAME && \
#        echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/$USERNAME && \
#        chmod 0440 /etc/sudoers.d/$USERNAME && \
#        # Replace 1000 with your user/group id
#        usermod  --uid 1000 $USERNAME && \
#        groupmod --gid 1000 $USERNAME

# Create user with same uid as host machine.
RUN useradd --create-home --shell /bin/bash --base-dir /home --uid ${uid} ${user}

RUN usermod -a -G dialout,video,audio ${user}

RUN apt-get update && apt-get install -y \
    sudo \
    python-dev \
    python-numpy \
    python-pip \
    python3-pip \
    vim \
    ipython \
    git \
    ssh \
    net-tools \
    iputils-ping \
    ros-$ROS_DISTRO-dynamixel-motor \
    ros-$ROS_DISTRO-joy \
    ros-$ROS_DISTRO-usb-cam \
    ros-$ROS_DISTRO-ros-control \
    ros-$ROS_DISTRO-ros-controllers

RUN usermod -aG sudo ${user} && \
    echo "${user} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/${user} && \
    chmod 0440 /etc/sudoers.d/${user}

RUN pip install --upgrade pip

RUN pip2.7 install --upgrade \
    jupyter \
    flake8 \
    flake8-copyright

# python 3.6
RUN apt-get install -y \
    software-properties-common

RUN add-apt-repository ppa:deadsnakes/ppa

RUN apt-get update && apt-get install -y \
    python3.6

RUN python3.6 -m pip install ipykernel

# later this will be mapped in by the run_docker script to the user specified ROS workspace.
ADD . /home/${user}/ros
ADD . /home/${user}/data
RUN export DATA_DIR=/home/${user}/data
ADD . /home/${user}/workspace

ADD ./bin/init.sh /home/${user}

RUN apt-get update && apt-get upgrade -y

USER ${user}

RUN python3.6 -m ipykernel install --user

WORKDIR /home/${user}/ros

RUN echo "source /opt/ros/$ROS_DISTRO/setup.bash" >> /home/${user}/.bashrc
RUN echo "source /home/${user}/ros/devel/setup.bash" >> /home/${user}/.bashrc
